-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 03 Bulan Mei 2020 pada 07.30
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `peminjaman_alat`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang`
--

CREATE TABLE `barang` (
  `idbarang` varchar(128) NOT NULL,
  `namabarang` varchar(255) DEFAULT NULL,
  `idjenis` int(11) DEFAULT NULL,
  `idmerk` int(11) DEFAULT NULL,
  `harga` int(16) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  `insert_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `barang`
--

INSERT INTO `barang` (`idbarang`, `namabarang`, `idjenis`, `idmerk`, `harga`, `deskripsi`, `stok`, `gambar`, `insert_at`) VALUES
('BRG001', 'Tas Gunung Eiger', 1, 1, 50000, 'Excelsior 65 adalah tas carrier yang dirancang untuk kegiatan ekspedisi dan trekking berdurasi panjang (lebih dari 4 hari). Tas Excelsior merupakan produk unggulan Eiger yang memenangkan penghargaan Best Winner dalam ajang Good Design Indonesia pada tahun 2017 lalu. Kini, tas ini kembali hadir dengan bahan yang lebih ringan dan kuat serta fitur yang ditingkatkan seperti penutup carrier yang dapat dilepas pasang dan juga diperluas untuk dijadikan sebagai tas ransel mini berkapasitas 18 liter dan hip belt yang dapat disesuaikan dengan lebar panggul. Dilengkapi dengan teknologi backsystem Ergo Spino Dual Frame yang dapat membagi beban secara merata dan memiliki frame berpengatur ukuran torso yang dapat menjaga tulang belakang dari resiko cedera, serta ruang sirkulasi udara yang lebih luas. Bahan Robic dan Cordura dipakai di seluruh bagian badan luar carrier agar menjamin kekuatan carrier dan mengatasi pemakaian yang ekstrim.', 18, 'eiger.jpg', '2020-05-02 10:18:14'),
('BRG002', 'Sleeping Bag', 2, 2, 42000, 'Bagi Anda penggemar kegiatan outdoor, kantong tidur (sleeping bag) adalah salah satu kebutuhan utama yang tidak boleh dilewatkan. Maka dari itu, Eiger menghadirkan produk sleeping bag Lake Side untuk kenyamanan dan keamanan saat tidur di alam terbuka. Produk ini dapat menjaga tubuh tetap hangat ketika suhu menjadi dingin terutama ketika Anda sedang berada di alam terbuka, khususnya di daerah tropis dengan suhu 15', 7, 'active.jpg', '2020-05-02 10:18:14');

-- --------------------------------------------------------

--
-- Struktur dari tabel `inbox`
--

CREATE TABLE `inbox` (
  `idinbox` int(11) NOT NULL,
  `namapengirim` varchar(255) DEFAULT NULL,
  `emailpengirim` varchar(255) DEFAULT NULL,
  `subjek` varchar(255) DEFAULT NULL,
  `pesan` text DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `inbox`
--

INSERT INTO `inbox` (`idinbox`, `namapengirim`, `emailpengirim`, `subjek`, `pesan`, `time`) VALUES
(3, 'budi', 'budi@gmail.com', 'tes', 'tes kirim pesan', '2020-05-03 01:52:26');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_barang`
--

CREATE TABLE `jenis_barang` (
  `idjenis` int(11) NOT NULL,
  `jenis` varchar(255) DEFAULT NULL,
  `info_tambahan` text DEFAULT NULL,
  `insert_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `jenis_barang`
--

INSERT INTO `jenis_barang` (`idjenis`, `jenis`, `info_tambahan`, `insert_at`) VALUES
(1, 'Tas / Ransel', 'Jenis Tas / Ransel', '2020-05-02 03:41:27'),
(2, 'Sleeping Bag', 'N/A', '2020-05-02 03:59:33');

-- --------------------------------------------------------

--
-- Struktur dari tabel `member`
--

CREATE TABLE `member` (
  `idmember` varchar(128) NOT NULL,
  `namamember` varchar(255) DEFAULT NULL,
  `jeniskelamin` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `notelp` varchar(15) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `foto_ktp` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `member`
--

INSERT INTO `member` (`idmember`, `namamember`, `jeniskelamin`, `email`, `notelp`, `alamat`, `foto_ktp`, `status`, `create_at`) VALUES
('MBR001', 'Mahendra', 1, 'mahen@gmail.com', '08123123', 'JL. Gubeng Kertajaya No.114, Surabaya', 'tf.jpg', 1, '2020-05-02 04:20:26');

-- --------------------------------------------------------

--
-- Struktur dari tabel `merk_barang`
--

CREATE TABLE `merk_barang` (
  `idmerk` int(11) NOT NULL,
  `merk` varchar(255) DEFAULT NULL,
  `info_tambahan` text DEFAULT NULL,
  `insert_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `merk_barang`
--

INSERT INTO `merk_barang` (`idmerk`, `merk`, `info_tambahan`, `insert_at`) VALUES
(1, 'Eiger', 'N/A', '2020-05-02 03:44:21'),
(2, 'Active Era', 'N/A', '2020-05-02 03:59:55');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengaturan`
--

CREATE TABLE `pengaturan` (
  `idpengaturan` varchar(128) NOT NULL,
  `namapengaturan` varchar(255) DEFAULT NULL,
  `valuepengaturan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pengaturan`
--

INSERT INTO `pengaturan` (`idpengaturan`, `namapengaturan`, `valuepengaturan`) VALUES
('alamat_toko', 'Alamat Toko', 'JL.Medokan Asri No.31 Surabaya'),
('call_center', 'Call Center', '081237123'),
('denda_terlambat', 'Denda Keterlambatan / Hari', '10000'),
('email_center', 'Email Toko', 'omahadventure@gmail.com'),
('icon_app', 'Icon Aplikasi', 'fa fa-suitcase-rolling'),
('nama_aplikasi', 'Nama Aplikasi', 'Omah Adventure'),
('owner_toko', 'Pemilik Toko', 'Budi Sudarsono'),
('tentang_kami', 'Tentang Kami', 'A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengembalian`
--

CREATE TABLE `pengembalian` (
  `idpengembalian` int(11) NOT NULL,
  `kodetransaksi` varchar(128) DEFAULT NULL,
  `tgl_kembali` timestamp NOT NULL DEFAULT current_timestamp(),
  `denda` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `kodetransaksi` varchar(128) NOT NULL,
  `idmember` varchar(128) DEFAULT NULL,
  `tgl_pinjam` varchar(64) DEFAULT NULL,
  `tgl_kembali` varchar(128) DEFAULT NULL,
  `durasi` int(11) DEFAULT NULL,
  `diskon` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `status_pengembalian` int(11) NOT NULL,
  `admin` int(11) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`kodetransaksi`, `idmember`, `tgl_pinjam`, `tgl_kembali`, `durasi`, `diskon`, `total`, `status_pengembalian`, `admin`, `time`) VALUES
('TRX-0001', 'MBR001', '2020-05-03', '2020-05-01', 3, 0, 276000, 0, 1, '2020-05-03 02:22:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi_detail`
--

CREATE TABLE `transaksi_detail` (
  `iddetail` int(11) NOT NULL,
  `kodetransaksi` varchar(128) DEFAULT NULL,
  `idbarang` varchar(128) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `transaksi_detail`
--

INSERT INTO `transaksi_detail` (`iddetail`, `kodetransaksi`, `idbarang`, `jumlah`) VALUES
(4, 'TRX-0001', 'BRG001', 1),
(5, 'TRX-0001', 'BRG002', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `userrole`
--

CREATE TABLE `userrole` (
  `idrole` int(11) NOT NULL,
  `role` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `userrole`
--

INSERT INTO `userrole` (`idrole`, `role`) VALUES
(1, 'Admin'),
(2, 'Member');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `iduser` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `idrole` int(11) DEFAULT NULL,
  `is_aktif` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`iduser`, `nama`, `username`, `email`, `password`, `idrole`, `is_aktif`, `date_created`) VALUES
(1, 'Omah Adventure', 'admin', 'adminomahadventure@gmail.com', '$2y$10$hbEtfnIU5y4cgbc15m7jQevyafL9iGJcbVgDsdo2dVhaRzhkQwaeC', 1, 1, '2020-05-02 03:08:11'),
(2, 'Coba User', 'cobauser', 'cobauser@gmail.com', '$2y$10$mGZWsICiUT8cZAqo1JvPs.ROJxF6ZngqTI5.OxQNaFH.Pcbq5c4Li', 2, 1, '2020-05-02 04:30:59');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`idbarang`),
  ADD KEY `idjenis` (`idjenis`),
  ADD KEY `idmerk` (`idmerk`);

--
-- Indeks untuk tabel `inbox`
--
ALTER TABLE `inbox`
  ADD PRIMARY KEY (`idinbox`);

--
-- Indeks untuk tabel `jenis_barang`
--
ALTER TABLE `jenis_barang`
  ADD PRIMARY KEY (`idjenis`);

--
-- Indeks untuk tabel `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`idmember`),
  ADD UNIQUE KEY `idmember` (`idmember`);

--
-- Indeks untuk tabel `merk_barang`
--
ALTER TABLE `merk_barang`
  ADD PRIMARY KEY (`idmerk`);

--
-- Indeks untuk tabel `pengaturan`
--
ALTER TABLE `pengaturan`
  ADD PRIMARY KEY (`idpengaturan`);

--
-- Indeks untuk tabel `pengembalian`
--
ALTER TABLE `pengembalian`
  ADD PRIMARY KEY (`idpengembalian`),
  ADD KEY `kodetransaksi` (`kodetransaksi`);

--
-- Indeks untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`kodetransaksi`),
  ADD KEY `idmember` (`idmember`),
  ADD KEY `admin` (`admin`);

--
-- Indeks untuk tabel `transaksi_detail`
--
ALTER TABLE `transaksi_detail`
  ADD PRIMARY KEY (`iddetail`),
  ADD KEY `idbarang` (`idbarang`),
  ADD KEY `kodetransaksi` (`kodetransaksi`);

--
-- Indeks untuk tabel `userrole`
--
ALTER TABLE `userrole`
  ADD PRIMARY KEY (`idrole`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`iduser`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `idrole` (`idrole`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `inbox`
--
ALTER TABLE `inbox`
  MODIFY `idinbox` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `jenis_barang`
--
ALTER TABLE `jenis_barang`
  MODIFY `idjenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `merk_barang`
--
ALTER TABLE `merk_barang`
  MODIFY `idmerk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `pengembalian`
--
ALTER TABLE `pengembalian`
  MODIFY `idpengembalian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `transaksi_detail`
--
ALTER TABLE `transaksi_detail`
  MODIFY `iddetail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `userrole`
--
ALTER TABLE `userrole`
  MODIFY `idrole` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `barang`
--
ALTER TABLE `barang`
  ADD CONSTRAINT `barang_ibfk_1` FOREIGN KEY (`idjenis`) REFERENCES `jenis_barang` (`idjenis`),
  ADD CONSTRAINT `barang_ibfk_2` FOREIGN KEY (`idmerk`) REFERENCES `merk_barang` (`idmerk`);

--
-- Ketidakleluasaan untuk tabel `pengembalian`
--
ALTER TABLE `pengembalian`
  ADD CONSTRAINT `pengembalian_ibfk_1` FOREIGN KEY (`kodetransaksi`) REFERENCES `transaksi` (`kodetransaksi`);

--
-- Ketidakleluasaan untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`idmember`) REFERENCES `member` (`idmember`),
  ADD CONSTRAINT `transaksi_ibfk_2` FOREIGN KEY (`admin`) REFERENCES `users` (`iduser`);

--
-- Ketidakleluasaan untuk tabel `transaksi_detail`
--
ALTER TABLE `transaksi_detail`
  ADD CONSTRAINT `transaksi_detail_ibfk_2` FOREIGN KEY (`idbarang`) REFERENCES `barang` (`idbarang`),
  ADD CONSTRAINT `transaksi_detail_ibfk_3` FOREIGN KEY (`kodetransaksi`) REFERENCES `transaksi` (`kodetransaksi`);

--
-- Ketidakleluasaan untuk tabel `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`idrole`) REFERENCES `userrole` (`idrole`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
