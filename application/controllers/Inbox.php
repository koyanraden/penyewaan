<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Inbox extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        //set default
        $this->title = 'Inbox';
        $this->menu = 'inbox';
        $this->parent = '';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {
        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'namapengirim', 'label' => 'Nama'];
        $a_kolom[] = ['kolom' => 'emailpengirim', 'label' => 'Email'];
        $a_kolom[] = ['kolom' => 'subjek', 'label' => 'Subjek'];
        $a_kolom[] = ['kolom' => 'pesan', 'label' => 'Pesan', 'is_null' => true, 'type' => 'A'];
        $a_kolom[] = ['kolom' => 'time', 'label' => 'Waktu'];

        $this->a_kolom = $a_kolom;
    }
}
