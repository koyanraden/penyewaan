<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaksi extends MY_Controller
{
    public $user;

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        //load model
        $this->load->model('M_users', 'users');
        $this->load->model('M_transaksi', 'transaksi');
        $this->load->model('M_member', 'nasabah');
        $this->load->model('M_barang', 'barang');
        $this->load->model('M_transaksidetail', 'transaksidetail');

        //load library
        $this->load->library('cart');

        $this->user = $this->users->getBy(['username' => $this->session->userdata['username']])->row_array();
    }

    public function index()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', base_url());
        $this->breadcrumb->append_crumb('Transaksi', '#');
        $data['title'] = 'Filter Transaksi';
        $data['profile'] = 'Filter Transaksi';
        $data['user'] = $this->user;
        $this->template->load('template', 'transaksi/index', $data);

        if ($_POST) {
            $from_date = $this->input->post('from_date');
            $to_date = $this->input->post('to_date');

            $from = new DateTime($from_date);
            $to = new DateTime($to_date);

            $diff = $to->diff($from)->days;
            $x = $from_date . '_' . $to_date . '_' . strval($diff);
            $enc = bin2hex(openssl_encrypt($x, 'AES-128-CBC', "key"));

            redirect('transaksi/trans/' . $enc);
        }
    }

    public function trans($diff = null)
    {
        if (!empty($diff)) {
            try {
                $decode = openssl_decrypt(hex2bin($diff), 'AES-128-CBC', "key");
                $arr = explode("_", $decode);
            } catch (Exception $e) {
                redirect('transaksi');
            }
        } else {
            redirect('transaksi');
        }

        if (empty($decode)) {
            $this->destroyCart();
            redirect('transaksi');
        }

        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', base_url());
        $this->breadcrumb->append_crumb('Filter Transaksi', base_url('transaksi'));
        $this->breadcrumb->append_crumb('Transaksi', '#');
        $data['title'] = 'Transaksi';
        $data['profile'] = 'Transaksi';
        $data['user'] = $this->user;
        $data['enc'] = $diff;
        $data['durasi'] = intval($arr[2]);
        $data['from_date'] = $arr[0];
        $data['to_date'] = $arr[1];
        $this->template->load('template', 'transaksi/trans', $data);

        if (!empty($_POST)) {
            $idbarang = $this->input->post('idbarang');
            $jumlah = $this->input->post('jumlah');
            $act = $this->input->post('act');
            $key = $this->input->post('key');

            $barang = $this->db->get_where('barang', ['idbarang' => $idbarang])->row_array();

            if ($act == "update") {
                $data = [
                    'rowid' => $key,
                    'qty' => $jumlah,
                ];
                $this->cart->update($data);
            } else {
                $data = [
                    'id' => $idbarang,
                    'qty' => $jumlah,
                    'price' => $barang['harga'],
                    'name' => $barang['namabarang'],
                ];
                $this->cart->insert($data);
            }

            redirect('transaksi/trans/' . $diff);
        }
    }

    public function getBarang()
    {
        $keyword = $this->input->post('cari');
        $query = "SELECT * FROM barang WHERE lower(idbarang) LIKE '%$keyword%' OR lower(namabarang) LIKE '%$keyword%'";
        $barang = $this->db->query($query);

        if ($barang->num_rows() < 1) {
            echo json_encode(array());
        } else {
            $data = array();
            foreach ($barang->result_array() as $val) {
                $data[] = array('id' => $val['idbarang'], 'text' => $val['idbarang'] . ' - ' . $val['namabarang']);
            }
            echo json_encode($data);
        }
    }

    public function getMember()
    {
        $keyword = $this->input->post('cari');
        $query = "SELECT * FROM member WHERE lower(idmember) LIKE '%$keyword%' OR lower(namamember) LIKE '%$keyword%'";
        $member = $this->db->query($query);

        if ($member->num_rows() < 1) {
            echo json_encode(array());
        } else {
            $data = array();
            foreach ($member->result_array() as $val) {
                $data[] = array('id' => $val['idmember'], 'text' => $val['idmember'] . ' - ' . $val['namamember']);
            }
            echo json_encode($data);
        }
    }

    public function destroyCart($enc = null)
    {
        $ok = $this->cart->destroy();
        !$ok ? setMessage('Berhasil membersihkan cart', 'success') : setMessage('Gagal membersihkan cart', 'danger');
        if (empty($enc)) {
            redirect('transaksi');
        } else {
            redirect('transaksi/trans/' . $enc);
        }
    }

    public function deleteCart($rowid, $enc)
    {
        $this->cart->remove($rowid);
        redirect('transaksi/trans/' . $enc);
    }

    public function checkout()
    {
        if (!empty($_POST)) {
            $notrans = $this->input->post('kodetrans');
            $from_date = $this->input->post('from_date');
            $to_date = $this->input->post('to_date');
            $member = $this->input->post('member');
            $total_sewa = $this->input->post('total_sewa');
            $hash = $this->input->post('hash');
            $admin = $this->user['iduser'];
            $cart = $this->cart->contents();

            $from = new DateTime($from_date);
            $to = new DateTime($to_date);

            $d = $to->diff($from)->days;

            $a_trans = [
                'kodetransaksi' => $notrans,
                'idmember' => $member,
                'tgl_pinjam' => $from_date,
                'tgl_kembali' => $to_date,
                'durasi' => $d,
                'diskon' => 0,
                'total' => $total_sewa,
                'status_pengembalian' => 0,
                'admin' => $admin
            ];

            $this->transaksi->beginTrans();
            $this->transaksi->insert($a_trans);

            foreach ($cart as $val) {
                $a_detail = [
                    'kodetransaksi' => $notrans,
                    'idbarang' => $val['id'],
                    'jumlah' => $val['qty']
                ];

                $stok = getBarang()[$val['id']]['stok'];

                $this->db->update('barang', ['stok' => $stok - $val['qty']], ['idbarang' => $val['id']]);
                $this->transaksidetail->insert($a_detail);
            }


            $ok = $this->transaksi->statusTrans();
            $this->transaksi->commitTrans($ok);

            if ($ok) {
                $this->cart->destroy();
                setMessage('Berhasil menyimpan data transaksi', 'success');
                redirect('transaksi');
            } else {
                setMessage('Gagal menyimpan data', 'danger');
                redirect('transaksi/trans/' . $hash);
            }
        }
    }

    public function riwayat()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', base_url());
        $this->breadcrumb->append_crumb('Riwayat Transaksi', '#');
        $data['title'] = 'Riwayat Transaksi';
        $data['profile'] = 'Transaksi';
        $data['user'] = $this->user;
        $data['riwayat'] = $this->transaksi->getTransaksi()->result_array();
        $this->template->load('template', 'transaksi/riwayat', $data);
    }

    public function batalkan($id)
    {
        $a_detail = $this->db->get_where('transaksi_detail', ['kodetransaksi' => $id])->result_array();

        foreach ($a_detail as $val) {
            $stok = getBarang()[$val['idbarang']]['stok'];
            $this->db->update('barang', ['stok' => $stok + $val['jumlah']], ['idbarang' => $val['idbarang']]);
            $this->db->delete('transaksi_detail', ['iddetail' => $val['iddetail']]);
        }

        $ok = $this->db->delete('transaksi', ['kodetransaksi' => $id]);

        $ok ? setMessage('Berhasil membatalkan transaksi', 'success') : setMessage('Gagal membatalkan trasaksi', 'danger');
        redirect('transaksi/riwayat');
    }

    public function detail($id)
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', base_url());
        $this->breadcrumb->append_crumb('Riwayat Transaksi', base_url('transaksi/riwayat'));
        $this->breadcrumb->append_crumb('Detail', '#');
        $data['title'] = 'Detail Transaksi';
        $data['profile'] = 'Detail Transaksi';
        $data['user'] = $this->user;
        $data['detail'] = $this->transaksidetail->getReff($id)->result_array();
        $this->template->load('template', 'transaksi/detail', $data);
    }
}
