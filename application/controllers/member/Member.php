<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Member extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        //set default
        $this->title = 'Data Member';
        $this->menu = 'member';
        $this->parent = 'member';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {
        $a_jeniskelamin = [
            '1' => 'Laki-Laki',
            '0' => 'Perempuan'
        ];

        $a_status = [
            '1' => 'Aktif',
            '0' => 'Tidak Aktif'
        ];

        $a_kolom = [];
        $a_kolom[] = ['kolom' => 'idmember', 'label' => 'ID Member'];
        $a_kolom[] = ['kolom' => 'namamember', 'label' => 'Nama Lengkap'];
        $a_kolom[] = ['kolom' => 'jeniskelamin', 'label' => 'Jenis Kelamin', 'type' => 'S', 'option' => $a_jeniskelamin];
        $a_kolom[] = ['kolom' => 'email', 'label' => 'Email', 'type' => 'E'];
        $a_kolom[] = ['kolom' => 'notelp', 'label' => 'No telp', 'type' => 'N'];
        $a_kolom[] = ['kolom' => 'foto_ktp', 'label' => 'Foto KTP', 'type' => 'F', 'file_type' => 'jpg|png|jpeg|gif', 'path' => './assets/img/member/'];
        $a_kolom[] = ['kolom' => 'alamat', 'label' => 'Alamat', 'type' => 'A', 'is_tampil' => false];
        $a_kolom[] = ['kolom' => 'status', 'label' => 'Status', 'type' => 'S', 'option' => $a_status];

        $this->a_kolom = $a_kolom;
    }
}
