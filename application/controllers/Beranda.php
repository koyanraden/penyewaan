<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Beranda extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_users', 'users');
    }

    public function index()
    {
        $data['title'] = 'Beranda';
        $data['active'] = 'beranda';
        $query = "SELECT * FROM barang JOIN jenis_barang USING(idjenis) LIMIT 4";
        $data['a_peralatan'] = $this->db->query($query)->result_array();
        $this->template->load('client/templatefront', 'client/home/index', $data);
    }

    public function about()
    {
        $data['title'] = 'Tentang Kami';
        $data['active'] = 'tentang';
        $this->template->load('client/templatefront', 'client/home/about', $data);
    }

    public function kontak()
    {
        $data['title'] = 'Kontak Kami';
        $data['active'] = 'kontak';
        $this->template->load('client/templatefront', 'client/home/kontak', $data);
    }
}
