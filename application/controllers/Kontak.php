<?php

class Kontak extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['title'] = 'Kontak';
        $data['active'] = 'kontak';
        $this->template->load('client/templatefront', 'client/kontak/index', $data);

        if ($_POST) {
            $nama = $this->input->post('nama');
            $email = $this->input->post('email');
            $subjek = $this->input->post('subjek');
            $pesan = $this->input->post('pesan');

            $a_data = [
                'namapengirim' => $nama,
                'emailpengirim' => $email,
                'subjek' => $subjek,
                'pesan' => $pesan
            ];

            $ok = $this->db->insert('inbox', $a_data);
            $ok ? setMessage('Berhasil mengirimkan pesan', 'success') : setMessage('Gagal mengirimkan pesan', 'danger');
            redirect('kontak');
        }
    }
}
