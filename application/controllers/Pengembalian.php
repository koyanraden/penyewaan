<?php

class Pengembalian extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_pengembalian', 'pengembalian');
    }

    public function index()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url());
        $this->breadcrumb->append_crumb('Pengembalian', '#');

        $data['title'] = 'Pengembalian Barang';
        $data['menu_now'] = 'Pengembalian';
        $data['user'] = $this->user;
        $data['a_data'] = $this->pengembalian->get()->result_array();
        $this->template->load('template', 'pengembalian/index', $data);

        if ($_POST) {
            $kodetrans = $this->input->post('kodetrans');
            $a_detail = $this->db->get_where('transaksi_detail', ['kodetransaksi' => $kodetrans])->result_array();

            $trans = $this->db->get_where('transaksi', ['kodetransaksi' => $kodetrans])->row_array();
            $diff = floor((strtotime($trans['tgl_kembali']) - time()) / (60 * 60 * 24) + 1);

            $denda = settingSIM()['denda_terlambat'];

            if ($diff < 0) {
                $denda *= ($diff * -1);
            } else {
                $denda = 0;
            }

            $a_data = [
                'kodetransaksi' => $kodetrans,
                'denda' => $denda
            ];

            $this->db->update('transaksi', ['status_pengembalian' => 1], ['kodetransaksi' => $kodetrans]);
            $ok = $this->db->insert('pengembalian', $a_data);

            foreach ($a_detail as $a) {
                $stok = getBarang()[$a['idbarang']]['stok'];
                $this->db->update('barang', ['stok' => $stok + $a['jumlah']], ['idbarang' => $a['idbarang']]);
            }

            $ok ? setMessage('Berhasil melakukan pengembalian barang!', 'success') : setMessage('Gagal mengembalikan barang!', 'danger');
            redirect('pengembalian');
        }
    }

    public function getKode()
    {
        $keyword = $this->input->post('cari');
        $query = "SELECT * FROM transaksi WHERE lower(kodetransaksi) LIKE '%$keyword%'";
        $transaksi = $this->db->query($query);

        if ($transaksi->num_rows() < 1) {
            echo json_encode(array());
        } else {
            $data = array();
            foreach ($transaksi->result_array() as $val) {
                $data[] = array('id' => $val['kodetransaksi'], 'text' => $val['kodetransaksi']);
            }
            echo json_encode($data);
        }
    }

    public function batalkan($id)
    {
        $a_detail = $this->db->get_where('transaksi_detail', ['kodetransaksi' => $id])->result_array();
        $up = $this->db->update('transaksi', ['status_pengembalian' => 0], ['kodetransaksi' => $id]);

        $is_updated_stok = false;

        foreach ($a_detail as $v) {
            $stok = getBarang()[$v['idbarang']]['stok'];
            $up_stok = $this->db->update('barang', ['stok' => $stok - $v['jumlah']], ['idbarang' => $v['idbarang']]);
            $is_updated_stok = $up_stok ? true : false;

            if ($is_updated_stok == false) {
                break;
            }
        }

        $del = $this->db->delete('pengembalian', ['kodetransaksi' => $id]);

        $up && $del && $is_updated_stok ? setMessage('Berhasil membatalkan pengembalian', 'success') : setMessage('Gagal membatalkan pengembalian', 'danger');
        redirect('pengembalian');
    }
}
