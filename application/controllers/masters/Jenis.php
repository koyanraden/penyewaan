<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jenis extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        //set default
        $this->title = 'Data Jenis';
        $this->menu = 'jenis';
        $this->parent = 'masters';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {
        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'jenis', 'label' => 'Jenis'];
        $a_kolom[] = ['kolom' => 'info_tambahan', 'label' => 'Info Tambahan', 'is_null' => true, 'type' => 'A'];

        $this->a_kolom = $a_kolom;
    }
}
