<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Barang extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->load->model('M_jenis', 'jenis');
        $this->load->model('M_merk', 'merk');

        //set default
        $this->title = 'Data Barang';
        $this->menu = 'barang';
        $this->parent = 'masters';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {

        $a_jenis = $this->jenis->getListCombo();
        $a_merk = $this->merk->getListCombo();


        $a_kolom = [];
        $a_kolom[] = ['kolom' => 'idbarang', 'label' => 'Kode Barang'];
        $a_kolom[] = ['kolom' => 'namabarang', 'label' => 'Nama'];
        $a_kolom[] = ['kolom' => 'idjenis', 'label' => 'Jenis', 'type' => 'S', 'option' => $a_jenis];
        $a_kolom[] = ['kolom' => 'idmerk', 'label' => 'Merk', 'type' => 'S', 'option' => $a_merk];
        $a_kolom[] = ['kolom' => 'gambar', 'label' => 'Gambar', 'type' => 'F', 'path' => './assets/img/barang/', 'file_type' => 'jpg|png|jpeg|gif'];
        $a_kolom[] = ['kolom' => 'harga', 'label' => 'Harga Sewa', 'type' => 'N', 'set_currency' => true];
        $a_kolom[] = ['kolom' => 'stok', 'label' => 'Stok', 'type' => 'N'];
        $a_kolom[] = ['kolom' => 'deskripsi', 'label' => 'Deskripsi', 'type' => 'A', 'is_tampil' => false];

        $this->a_kolom = $a_kolom;
    }
}
