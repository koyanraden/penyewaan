<?php

class Peralatan extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if ($_POST) {
            $keyword = $this->input->post('keyword');
            $query = "SELECT * FROM barang JOIN jenis_barang USING(idjenis) WHERE namabarang LIKE '%$keyword%'";
        } else {
            $query = "SELECT * FROM barang JOIN jenis_barang USING(idjenis)";
        }
        $data['title'] = 'Peralatan';
        $data['active'] = 'peralatan';
        $data['a_peralatan'] = $this->db->query($query)->result_array();
        $this->template->load('client/templatefront', 'client/peralatan/index', $data);
    }

    public function detail($id)
    {
        $data['title'] = 'Peralatan';
        $data['active'] = 'peralatan';
        $query = "SELECT * FROM barang JOIN jenis_barang USING(idjenis) WHERE idbarang='$id'";
        $data['a_peralatan'] = $this->db->query($query)->row_array();
        $this->template->load('client/templatefront', 'client/peralatan/detail', $data);
    }
}
