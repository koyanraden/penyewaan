<?php
defined('BASEPATH') or exit('No direct script access allowed');

class exceptions
{

    public function checkForError()
    {
        get_instance()->load->database();
        $error = get_instance()->db->error();
        if ($error['code'])
            throw new SQLException($error);
    }
}

abstract class UserException extends exception
{
    public abstract function getUserMessage();
}

class SQLException extends UserException
{
    private $errorNumber;
    public $errorMessage;

    public function __construct(array $error)
    {
        $this->errorNumber = "Error Code(" . $error['code'] . ")";
        $this->errorMessage = $error['message'];
    }

    public function getUserMessage()
    {
        return array(
            "error" => array(
                "code" => $this->errorNumber,
                "message" => $this->errorMessage
            )
        );
    }
}
