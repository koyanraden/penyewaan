<div id="content">
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <form action="<?= base_url('pengembalian') ?>" method="POST">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2 mt-2">
                                        <label for="kodetrans"><strong>Kode Transaksi</strong></label>
                                    </div>
                                    <div class="col-md-4">
                                        <select name="kodetrans" id="kodetrans" class="form-control" required></select>
                                    </div>
                                    <div class="col-md-4">
                                        <button class="btn btn-sm btn-success" type="submit">Kembalikan</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <?= $this->session->flashdata('message'); ?>
                        <div class="table-responsive">
                            <table width="100%" class="table table-hover table-striped pengaturan">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Kode Transaksi</th>
                                        <th scope="col">Tgl Kembali</th>
                                        <th scope="col">Denda</th>
                                        <th scope="col">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    <?php foreach ($a_data as $data) : ?>
                                        <tr>
                                            <td><?= $i++; ?></td>
                                            <td><?= $data['kodetransaksi']; ?></td>
                                            <td><?= $data['tgl_kembali']; ?></td>
                                            <td><?= toRupiah($data['denda']); ?></td>
                                            <td>
                                                <a href="<?= base_url('transaksi/detail/' . $data['kodetransaksi']) ?>" class="btn btn-sm btn-info" data-id="<?= $data['kodetransaksi'] ?>" data-type="btn-detail">Detail</a>
                                                <button class="btn btn-sm btn-danger" data-id="<?= $data['kodetransaksi'] ?>" data-type="btn-batalkan">Batalkan</button>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of Main Content -->

            <script>
                $('.pengembalian').DataTable();
                $('#kodetrans').select2({
                    minimumInputLength: 3,
                    allowClear: true,
                    placeholder: 'Masukkan kode transaksi',
                    ajax: {
                        dataType: 'json',
                        type: 'POST',
                        url: '<?= site_url('pengembalian/getKode/') ?>',
                        delay: 250,
                        data: function(params) {
                            return {
                                cari: params.term
                            }
                        },
                        processResults: function(data, page) {
                            return {
                                results: data
                            };
                        },
                    }
                });

                $(function() {
                    $('[data-type="btn-batalkan"]').click(function() {
                        var id = $(this).attr('data-id');
                        bootbox.confirm("Apakah anda ingin membatalkan pengembalian?", function(result) {
                            if (result) {
                                location.href = '<?= base_url('pengembalian/batalkan/') ?>' + id;
                            }
                        });

                    })
                });
            </script>
            <!-- End of Main Content -->
        </div>
    </div>
</div>