<!-- Main Content -->
<div id="content">
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <h3 class="mb-4 text-dark"><b><?= $title; ?></b></h3>
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12">
                        <?php if (validation_errors()) : ?>
                            <div class="alert alert-danger" role="alert">
                                <?= validation_errors(); ?>
                            </div>
                        <?php endif; ?>
                        <?= $this->session->flashdata('message'); ?>
                        <table class="table" id="tabel-riwayat">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Kode Transaksi</th>
                                    <th scope="col">Barang</th>
                                    <th scope="col">Harga</th>
                                    <th scope="col">Durasi</th>
                                    <th scope="col">Jumlah</th>
                                    <th scope="col">Subtotal</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1;
                                $grandTotal = 0;
                                ?>
                                <?php foreach ($detail as $s) : ?>
                                    <tr>
                                        <td><?= $i++ ?></td>
                                        <td><?= $s['kodetransaksi']; ?></td>
                                        <td><?= $s['namabarang']; ?></td>
                                        <td><?= toRupiah($s['harga']); ?></td>
                                        <td><?= $s['durasi']; ?> Hari</td>
                                        <td><?= $s['jumlah']; ?> Unit</td>
                                        <td><?= toRupiah($s['harga'] * $s['durasi'] * $s['jumlah']); ?></td>
                                    </tr>
                                    <?php $grandTotal += $s['harga'] * $s['durasi'] * $s['jumlah'] ?>
                                    <?php $i++; ?>
                                <?php endforeach; ?>
                                <?php if (count($detail) < 1) : ?>
                                    <tr>
                                        <td colspan="7" align="center" class="text-danger">Detail tidak ditemukan</td>
                                    </tr>
                                <?php endif; ?>
                                <tr>
                                    <td colspan="6" class="table-info" align="right">Diskon :</td>
                                    <td class="table-info"><?= $detail[0]['diskon'] ?>%</td>
                                </tr>
                                <tr>
                                    <td colspan="6" class="table-info" align="right">Grand Total :</td>
                                    <td class="table-info"><?= toRupiah($grandTotal) ?></td>
                                </tr>
                            </tbody>
                        </table>
                        <input type="hidden" name="act" id="act">
                        <input type="hidden" name="key" id="key">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<script>
    $('[data-type=detail]').click(function() {
        var id = $(this).attr('data-id');
        location.href = `<?= base_url('transaksi/detail/') ?>${id}`;
    });

    $(function() {
        $('[data-type="cancel"]').click(function() {
            var id = $(this).attr('data-id');
            bootbox.confirm("Apakah anda ingin membatalkan transaksi?", function(result) {
                if (result) {
                    location.href = '<?= base_url('transaksi/batalkan/') ?>' + id;
                }
            });

        })
    });
</script>