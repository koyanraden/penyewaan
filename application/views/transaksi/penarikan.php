<!-- Main Content -->
<div id="content">
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <h3 class="mb-4 text-dark"><b><?= $title; ?></b></h3>
        <div class="row d-flex justify-content-center">
            <div class="col-md-6">
                <div class="card shadow mb-4">
                    <div class="card-body">
                        <?= $this->session->flashdata('message') ?>
                        <form action="<?= base_url('nasabah/penarikantabungan') ?>" method="POST">
                            <div class="form-group">
                                <label for="">Kode Penarikan</label>
                                <select name="kodepenarikan" id="kodepenarikan" class="form-control" required></select>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit">Tarik Saldo</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->

<script>
    $('#kodepenarikan').select2({
        minimumInputLength: 3,
        allowClear: true,
        placeholder: 'Masukkan kodepenarikan',
        ajax: {
            dataType: 'json',
            type: 'POST',
            url: '<?= site_url('nasabah/penarikantabungan/getPenarikan') ?>',
            delay: 250,
            data: function(params) {
                return {
                    cari: params.term
                }
            },
            processResults: function(data, page) {
                return {
                    results: data
                };
            },
        }
    });
</script>