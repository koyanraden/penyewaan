<div id="content">
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><b><?= $title; ?></b></h1>

        <div class="d-flex justify-content-center">
            <div class="col-md-6">
                <div class="card shadow mb-4">
                    <div class="card-body">
                        <?= $this->session->flashdata('message') ?>
                        <form action="<?= base_url('transaksi') ?>" method="POST">
                            <div class="form-group">
                                <label for="from_date">Tgl Peminjaman</label>
                                <input type="date" name="from_date" id="from_date" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="to_date">Tgl Pengembalian</label>
                                <input type="date" name="to_date" id="to_date" class="form-control" required>
                            </div>
                            <button class="btn btn-sm btn-primary" type="submit">Lanjutkan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

</script>