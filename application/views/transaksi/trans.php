<div id="content">
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><b><?= $title; ?></b></h1>

        <div class="card shadow mb-4">
            <div class="card-body">
                <form action="<?= base_url('transaksi/checkout') ?>" method="POST" id="form-checkout">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3 mt-2">
                                        <label for="notrans"><strong>Kode Transaksi</strong></label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="kodetrans" id="kodetrans" placeholder="No Transaksi" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3 mt-2">
                                        <label for="member"><strong>Member</strong></label>
                                    </div>
                                    <div class="col-md-6">
                                        <select name="member" id="member" class="form-control" required></select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="hash" id="hash" value="<?= $enc ?>">
                        <input type="hidden" name="from_date" id="from_date" value="<?= $from_date ?>">
                        <input type="hidden" name="to_date" id="to_date" value="<?= $to_date ?>">
                        <input type="hidden" name="total_sewa" id="total_sewa">
                    </div>
                    <div class="mb-2">
                        <hr>
                    </div>
                    <div class="mb-3">
                        <button type="button" class="btn btn-sm btn-success" data-type="btnsimpan" id="btn-simpan"><i class="fa fa-print mr-1"></i>Simpan</button>
                        <a href="<?= base_url('transaksi/destroyCart/' . $enc) ?>" class="btn btn-sm btn-danger"><i class="fa fa-trash mr-1"></i> Kosongkan Keranjang</a>
                    </div>
                </form>
                <div class="mt-2">
                    <?= $this->session->flashdata('message') ?>
                </div>
                <table class="table">
                    <thead>
                        <th>No</th>
                        <th>Nama Barang</th>
                        <th>Harga</th>
                        <th>Durasi Sewa</th>
                        <th>Jumlah</th>
                        <th>Subtotal</th>
                        <th>Aksi</th>
                    </thead>
                    <tbody>
                        <form action="<?= base_url('transaksi/trans/' . $enc) ?>" method="POST">
                            <tr>
                                <td colspan="4"><select name="idbarang" id="idbarang" class="form-control" required></select></td>
                                <td colspan="3"><input type="number" class="form-control" name="jumlah" id="jumlah" placeholder="jumlah" required></td>
                                <td>
                                    <button type="submit" class="btn btn-sm btn-success">Tambah</button>
                                </td>
                            </tr>
                        </form>
                        <?php $total = 0;
                        $i = 1; ?>
                        <?php foreach ($this->cart->contents() as $val) : ?>
                            <tr>
                                <td><?= $i++ ?></td>
                                <td><?= $val['name'] ?></td>
                                <td><?= toRupiah($val['price']) ?></td>
                                <td><?= $durasi ?> Hari</td>
                                <td><?= $val['qty'] ?> Unit</td>
                                <td><?= toRupiah($val['qty'] * $val['price'] * $durasi) ?></td>
                                <td>
                                    <button data-type="btn-update" data-id="<?= $val['rowid'] ?>" data-jumlah="<?= $val['qty'] ?>" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></button>
                                    <a href="<?= base_url('transaksi/deleteCart/' . $val['rowid'] . '/' . $enc) ?>" class="btn btn-sm btn-danger"><i class="fa fa-trash-alt"></i></a>
                                </td>
                            </tr>
                            <?php $total += ($val['qty'] * $val['price'] * $durasi) ?>
                        <?php endforeach; ?>
                        <tr class="table table-primary">
                            <td colspan="5" align="right"><b>Total</b></td>
                            <td colspan="3"><?= toRupiah($total); ?></td>
                            <input type="hidden" name="total_awal" id="total_awal" value="<?= $total ?>">
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="newMenuModal" tabindex="-1" role="dialog" aria-labelledby="newMenuModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newMenuModalLabel">Update</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('transaksi/trans/' . $enc) ?>" method="post" id="modal_post">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="qty">Jumlah</label>
                        <input type="number" class="form-control" id="jumlah" name="jumlah" placeholder="Jumlah" required>
                    </div>
                </div>
                <input type="hidden" id="act" name="act">
                <input type="hidden" id="key" name="key">
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" data-type="simpan" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    let cartCount = '<?= count($this->cart->contents()) ?>';
    let btnSimpan = $('#btn-simpan');

    if (cartCount > 0) {
        btnSimpan.show();
    } else {
        btnSimpan.hide();
    }

    $('[data-type=btnsimpan]').click(function() {
        let total_awal = $('#total_awal').val();
        let totalSewa = $('#total_sewa').val(total_awal);
        let kodeTrans = $('#kodetrans').val();
        let member = $('#member').val();

        if (kodeTrans == "" || member == "") {
            location.href = "<?= base_url('transaksi/trans/' . $enc) ?>";
        } else {
            $('#form-checkout').submit();
        }

    });


    $('#idbarang').select2({
        minimumInputLength: 3,
        allowClear: true,
        placeholder: 'Masukkan kode/nama barang',
        ajax: {
            dataType: 'json',
            type: 'POST',
            url: '<?= site_url('transaksi/getBarang/') ?>',
            delay: 250,
            data: function(params) {
                return {
                    cari: params.term
                }
            },
            processResults: function(data, page) {
                return {
                    results: data
                };
            },
        }
    });

    $('#member').select2({
        minimumInputLength: 3,
        allowClear: true,
        placeholder: 'Masukkan nama member',
        ajax: {
            dataType: 'json',
            type: 'POST',
            url: '<?= site_url('transaksi/getMember/') ?>',
            delay: 250,
            data: function(params) {
                return {
                    cari: params.term
                }
            },
            processResults: function(data, page) {
                return {
                    results: data
                };
            },
        }
    });

    $('[data-type=btn-update]').click(function() {
        let id = $(this).attr('data-id');
        let qty = $(this).attr('data-jumlah');
        const modal = $('#newMenuModal');
        modal.find('#jumlah').val(qty);
        modal.find('#key').val(id);
        modal.find('#act').val('update');
        modal.modal();
    });
</script>