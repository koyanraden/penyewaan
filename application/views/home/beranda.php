<!-- Main Content -->
<div id="content">
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-6">
                <?= $this->session->flashdata('message'); ?>
            </div>
        </div>
        <div class="jumbotron">
            <h1 class="display-4">Selamat datang, <?= $user['username']; ?></h1>
            <p class="lead">Admin Panel Produk Desaku</p>
        </div>
        <!-- Content Row -->
        <div class="row">
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2" id="pelanggaransiswa">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Jumlah Desa</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $desa->num_rows(); ?></div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-archway fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2" id="siswa">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Jumlah Barang</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $produk->num_rows(); ?></div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-box fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2" id="guru">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Jumlah Transaksi</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $pembayaran->num_rows(); ?></div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-cart-arrow-down fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- End of Main Content -->
</div>
<script>
    $('#siswa').click(function() {
        location.href = '<?= site_url('master') ?>';
    });
    $('#guru').click(function() {
        location.href = '<?= site_url('master/guru') ?>';
    });
    $('#pelanggaransiswa').click(function() {
        location.href = '<?= site_url('manajemen') ?>';
    });
</script>