<?php

$app_name = settingSIM()['nama_aplikasi'];
$name = explode(" ", $app_name);

?>
<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href="<?= base_url('beranda') ?>"><?= $name[0] ?><span><?= $name[1] ?></span></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item <?= $active == 'beranda' ? 'active' : '' ?>"><a href="<?= base_url('beranda') ?>" class="nav-link">Beranda</a></li>
                <li class="nav-item <?= $active == 'tentang' ? 'active' : '' ?>"><a href="<?= base_url('beranda/about') ?>" class="nav-link">Tentang Kami</a></li>
                <li class="nav-item <?= $active == 'peralatan' ? 'active' : '' ?>"><a href="<?= base_url('peralatan') ?>" class="nav-link">Peralatan</a></li>
                <li class="nav-item <?= $active == 'kontak' ? 'active' : '' ?>"><a href="<?= base_url('kontak') ?>" class="nav-link">Kontak</a></li>
                <li class="nav-item <?= $active == 'auth' ? 'active' : '' ?>"><a href="<?= base_url('auth') ?>" class="nav-link">Admin</a></li>
            </ul>
        </div>
    </div>
</nav>