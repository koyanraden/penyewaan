<!DOCTYPE html>
<html lang="en">

<head>
    <title><?= $title . ' | ' . settingSIM()['nama_aplikasi'] ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="<?= base_url('assets/client/css/open-iconic-bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/client/css/animate.css') ?>">

    <link rel="stylesheet" href="<?= base_url('assets/client/css/owl.carousel.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/client/css/owl.theme.default.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/client/css/magnific-popup.css') ?>">

    <link rel="stylesheet" href="<?= base_url('assets/client/css/aos.css') ?>">

    <link rel="stylesheet" href="<?= base_url('assets/client/css/ionicons.min.css') ?>">

    <link rel="stylesheet" href="<?= base_url('assets/client/css/bootstrap-datepicker.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/client/css/jquery.timepicker.css') ?>">


    <link rel="stylesheet" href="<?= base_url('assets/client/css/flaticon.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/client/css/icomoon.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/client/css/style.css') ?>">
</head>

<body>