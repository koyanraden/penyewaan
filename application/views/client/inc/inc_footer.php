<footer class="ftco-footer ftco-bg-dark ftco-section">
    <div class="container">
        <div class="row mb-5">
            <div class="col-md">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2">Tentang <?= settingSIM()['nama_aplikasi'] ?></h2>
                    <p><?= settingSIM()['tentang_kami'] ?></p>
                    <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                        <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                        <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                        <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2">Ada Pertanyaan?</h2>
                    <div class="block-23 mb-3">
                        <ul>
                            <li><span class="icon icon-map-marker"></span><span class="text"><?= settingSIM()['alamat_toko'] ?></span></li>
                            <li><a href="#"><span class="icon icon-phone"></span><span class="text"><?= settingSIM()['call_center'] ?></span></a></li>
                            <li><a href="#"><span class="icon icon-envelope"></span><span class="text"><?= settingSIM()['email_center'] ?></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">

                <p>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright &copy;<script>
                        document.write(new Date().getFullYear());
                    </script> All rights reserved | This template is made with <i class="icon-heart color-danger" aria-hidden="true"></i> by <a href="<?= base_url('beranda') ?>" target="_blank"><?= settingSIM()['nama_aplikasi'] ?></a>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                </p>
            </div>
        </div>
    </div>
</footer>



<!-- loader -->
<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
        <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
        <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00" /></svg></div>


<script src="<?= base_url('assets/client/js/jquery.min.js') ?>"></script>
<script src="<?= base_url('assets/client/js/jquery-migrate-3.0.1.min.js') ?>"></script>
<script src="<?= base_url('assets/client/js/popper.min.js') ?>"></script>
<script src="<?= base_url('assets/client/js/bootstrap.min.js') ?>"></script>
<script src="<?= base_url('assets/client/js/jquery.easing.1.3.js') ?>"></script>
<script src="<?= base_url('assets/client/js/jquery.waypoints.min.js') ?>"></script>
<script src="<?= base_url('assets/client/js/jquery.stellar.min.js') ?>"></script>
<script src="<?= base_url('assets/client/js/owl.carousel.min.js') ?>"></script>
<script src="<?= base_url('assets/client/js/jquery.magnific-popup.min.js') ?>"></script>
<script src="<?= base_url('assets/client/js/aos.js') ?>"></script>
<script src="<?= base_url('assets/client/js/jquery.animateNumber.min.js') ?>"></script>
<script src="<?= base_url('assets/client/js/bootstrap-datepicker.js') ?>"></script>
<script src="<?= base_url('assets/client/js/jquery.timepicker.min.js') ?>"></script>
<script src="<?= base_url('assets/client/js/scrollax.min.js') ?>"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
<script src="<?= base_url('assets/client/js/google-map.js') ?>"></script>
<script src="<?= base_url('assets/client/js/main.js') ?>"></script>

</body>

</html>