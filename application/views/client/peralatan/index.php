<section class="hero-wrap hero-wrap-2 js-fullheight" style="background-image: url('<?= base_url('assets/img/konten/adventure2.jpg') ?>');" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-end justify-content-start">
            <div class="col-md-9 ftco-animate pb-5">
                <p class="breadcrumbs"><span class="mr-2"><a href="<?= base_url('beranda') ?>">Beranda <i class="ion-ios-arrow-forward"></i></a></span> <span>Peralatan <i class="ion-ios-arrow-forward"></i></span></p>
                <h1 class="mb-3 bread">Peralatan</h1>
            </div>
        </div>
    </div>
</section>
<section class="ftco-section">
    <div class="container-fluid px-4">
        <div class="row justify-content-center">
            <div class="col-md-12 heading-section text-center ftco-animate mb-5">
                <span class="subheading">Yang Kami Tawarkan</span>
                <h2 class="mb-2">Pilih Peralatan Kamu</h2>
            </div>
        </div>
        <div class="row d-flex justify-content-center mb-5">
            <div class="col-md-6">
                <form action="<?= base_url('peralatan') ?>" method="POST">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" name="keyword" id="keyword" placeholder="Ketikkan barang yang kamu cari ..." aria-label="Recipient's username" aria-describedby="button-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit" id="button-addon2">Cari</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row d-flex justify-content-center">
            <?php foreach ($a_peralatan as $alat) : ?>
                <div class="col-md-3">
                    <div class="car-wrap ftco-animate">
                        <div class="img d-flex align-items-end" style="background-image: url(<?= base_url('assets/img/barang/' . $alat['gambar']) ?>);">
                            <div class="price-wrap d-flex">
                                <span class="rate"><?= toRupiah($alat['harga']); ?></span>
                                <p class="from-day">
                                    <span>Per</span>
                                    <span>/Hari</span>
                                </p>
                            </div>
                        </div>
                        <div class="text p-4 text-center">
                            <h2 class="mb-0"><a href="#"><?= $alat['namabarang'] ?></a></h2>
                            <span><?= $alat['jenis'] ?></span>
                            <p>Stok : <?= $alat['stok'] ?></p>
                            <p>Status : <?= $alat['stok'] > 0 ? '<font class="text-success">Tersedia</font>' : '<font class="text-danger">Tidak Tersedia</font>' ?>
                            </p>
                            <a href="<?= base_url('peralatan/detail/' . $alat['idbarang']) ?>" class="btn btn-block btn-primary">Detail</a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>