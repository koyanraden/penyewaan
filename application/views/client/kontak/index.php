<section class="hero-wrap hero-wrap-2 js-fullheight" style="background-image: url('<?= base_url('assets/img/konten/adventure2.jpg') ?>');" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-end justify-content-start">
            <div class="col-md-9 ftco-animate pb-5">
                <p class="breadcrumbs"><span class="mr-2"><a href="<?= base_url('beranda') ?>">Beranda <i class="ion-ios-arrow-forward"></i></a></span> <span>Kontak kami <i class="ion-ios-arrow-forward"></i></span></p>
                <h1 class="mb-3 bread">Kontak Kami</h1>
            </div>
        </div>
    </div>
</section>
<section class="ftco-section contact-section">
    <div class="container">
        <div class="row d-flex mb-5 contact-info justify-content-center">
            <div class="col-md-8">
                <div class="row mb-5">
                    <div class="col-md-4 text-center py-4">
                        <div class="icon">
                            <span class="icon-map-o"></span>
                        </div>
                        <p><span>Alamat:</span> <?= settingSIM()['alamat_toko'] ?></p>
                    </div>
                    <div class="col-md-4 text-center border-height py-4">
                        <div class="icon">
                            <span class="icon-mobile-phone"></span>
                        </div>
                        <p><span>Telepon:</span> <?= settingSIM()['call_center'] ?></a></p>
                    </div>
                    <div class="col-md-4 text-center py-4">
                        <div class="icon">
                            <span class="icon-envelope-o"></span>
                        </div>
                        <p><span>Email:</span> <a href="mailto:<?= settingSIM()['email_center'] ?>"><?= settingSIM()['email_center'] ?></a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row block-9 justify-content-center mb-5">
            <div class="col-md-8 mb-md-5">
                <h2 class="text-center">Jika ada pertanyaan <br>mohon tinggalkan pesan disini</h2>
                <?= $this->session->flashdata('message') ?>
                <form action="<?= base_url('kontak') ?>" class="bg-light p-5 contact-form" method="POST">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Nama Kamu" name="nama" required>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Email Kamu" name="email" required>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Subjek" name="subjek" required>
                    </div>
                    <div class="form-group">
                        <textarea name="pesan" id="" cols="30" rows="7" class="form-control" placeholder="Pesan" required></textarea>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Send Message" class="btn btn-primary py-3 px-5">
                    </div>
                </form>

            </div>
        </div>
    </div>
</section>