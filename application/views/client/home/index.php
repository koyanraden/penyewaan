<div class="hero-wrap" style="background-image: url('<?= base_url('assets/img/konten/travel.jpg') ?>');" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text justify-content-start align-items-center">
            <div class="col-lg-6 col-md-6 ftco-animate d-flex align-items-end">
                <div class="text">
                    <h1 class="mb-4">Sekarang <span>mudah untukmu</span> <span>menyewa peralatan</span></h1>
                    <p style="font-size: 18px;">Petualangan adalah sebuah pengalaman yang tak lazim, tetapi sifatnya menarik. Pelaku petualangan disebut petualang, yaitu orang yang suka mencari pengalaman yang sulit-sulit, berbahaya dan lain-lain.</p>
                    <a href="https://www.youtube.com/watch?v=x6xOCAamyxI" class="icon-wrap popup-vimeo d-flex align-items-center mt-4">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="ion-ios-play"></span>
                        </div>
                        <div class="heading-title ml-5">
                            <span>Sewa peralatan dengan mudah</span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="ftco-section services-section ftco-no-pt ftco-no-pb mt-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 heading-section text-center ftco-animate mb-5">
                <span class="subheading">Pelayanan Kami</span>
                <h2 class="mb-2">Pelayanan Kami</h2>
            </div>
        </div>
        <div class="row d-flex justify-content-center">
            <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services">
                    <div class="media-body py-md-4">
                        <div class="d-flex mb-3 align-items-center">
                            <div class="icon"><span class="flaticon-customer-support"></span></div>
                            <h3 class="heading mb-0 pl-3">24/7 Car Support</h3>
                        </div>
                        <p>A small river named Duden flows by their place and supplies it with you</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services">
                    <div class="media-body py-md-4">
                        <div class="d-flex mb-3 align-items-center">
                            <div class="icon"><span class="flaticon-route"></span></div>
                            <h3 class="heading mb-0 pl-3">Lots of location</h3>
                        </div>
                        <p>A small river named Duden flows by their place and supplies it with you</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services">
                    <div class="media-body py-md-4">
                        <div class="d-flex mb-3 align-items-center">
                            <div class="icon"><span class="flaticon-online-booking"></span></div>
                            <h3 class="heading mb-0 pl-3">Reservation</h3>
                        </div>
                        <p>A small river named Duden flows by their place and supplies it with you</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section">
    <div class="container-fluid px-4">
        <div class="row justify-content-center">
            <div class="col-md-12 heading-section text-center ftco-animate mb-5">
                <span class="subheading">Yang Kami Tawarkan</span>
                <h2 class="mb-2">Pilih Peralatan Kamu</h2>
            </div>
        </div>
        <div class="row d-flex justify-content-center">
            <?php foreach ($a_peralatan as $alat) : ?>
                <div class="col-md-3">
                    <div class="car-wrap ftco-animate">
                        <div class="img d-flex align-items-end" style="background-image: url(<?= base_url('assets/img/barang/' . $alat['gambar']) ?>);">
                            <div class="price-wrap d-flex">
                                <span class="rate"><?= toRupiah($alat['harga']); ?></span>
                                <p class="from-day">
                                    <span>Per</span>
                                    <span>/Hari</span>
                                </p>
                            </div>
                        </div>
                        <div class="text p-4 text-center">
                            <h2 class="mb-0"><a href="#"><?= $alat['namabarang'] ?></a></h2>
                            <span><?= $alat['jenis'] ?></span>
                            <p>Stok : <?= $alat['stok'] ?></p>
                            <p>Status : <?= $alat['stok'] > 0 ? '<font class="text-success">Tersedia</font>' : '<font class="text-danger">Tidak Tersedia</font>' ?>
                            </p>
                            <a href="<?= base_url('peralatan/detail/' . $alat['idbarang']) ?>" class="btn btn-block btn-primary">Detail</a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="text-center">
            <a href="<?= base_url('peralatan') ?>">
                <h5>Lihat Selengkapnya</h5>
            </a>
        </div>
    </div>
</section>

<section class="ftco-section ftco-no-pt ftco-no-pb mb-5">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-md-6 p-md-5 img img-2 d-flex justify-content-center align-items-center" style="background-image: url(<?= base_url('assets/img/adventure.jpg') ?>);">
            </div>
            <div class="col-md-6 wrap-about py-md-5 ftco-animate">
                <div class="heading-section mb-5 pl-md-5">
                    <span class="subheading">Tentang Kami</span>
                    <h2 class="mb-4"><?= settingSIM()['nama_aplikasi'] ?></h2>
                    <p><?= settingSIM()['tentang_kami'] ?></p>
                    <p><a href="<?= base_url('beranda/about') ?>" class="btn btn-primary">Lihat Selengkapnya</a></p>
                </div>
            </div>
        </div>
    </div>
</section>