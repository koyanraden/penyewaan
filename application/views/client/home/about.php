<section class="hero-wrap hero-wrap-2 js-fullheight" style="background-image: url('<?= base_url('assets/img/konten/adventure2.jpg') ?>');" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-end justify-content-start">
            <div class="col-md-9 ftco-animate pb-5">
                <p class="breadcrumbs"><span class="mr-2"><a href="<?= base_url('beranda') ?>">Beranda <i class="ion-ios-arrow-forward"></i></a></span> <span>Tentang kami <i class="ion-ios-arrow-forward"></i></span></p>
                <h1 class="mb-3 bread">Tentang Kami</h1>
            </div>
        </div>
    </div>
</section>
<section class="ftco-section ftco-no-pt ftco-no-pb mb-5 mt-5">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-md-6 p-md-5 img img-2 d-flex justify-content-center align-items-center" style="background-image: url(<?= base_url('assets/img/adventure.jpg') ?>);">
            </div>
            <div class="col-md-6 wrap-about py-md-5 ftco-animate">
                <div class="heading-section mb-5 pl-md-5">
                    <span class="subheading">Tentang Kami</span>
                    <h2 class="mb-4"><?= settingSIM()['nama_aplikasi'] ?></h2>
                    <p><?= settingSIM()['tentang_kami'] ?></p>
                    <p><a href="#" class="btn btn-primary">Lihat Selengkapnya</a></p>
                </div>
            </div>
        </div>
    </div>
</section>