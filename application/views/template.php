<?php include('templates/header.php') ?>
<?php include('templates/sidebar.php') ?>
<?php include('templates/topbar.php') ?>

<!-- Begin Page Content -->
<div class="container-fluid">
    <?php echo $this->breadcrumb->output(); ?>
</div>
<?php echo $contents; ?>
<?php include('templates/footer.php') ?>