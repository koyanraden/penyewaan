<!-- Main Content -->
<div id="content">
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

        <div class="card shadow mb-4">
            <div class="card-body">
                <!-- <a class="btn btn-primary mb-3" href="<?= $menu . '/create' ?>">Tambah <?= $title; ?></a> -->
                <div class="row">
                    <div class="col-md-12">
                        <form method="post" enctype="multipart/form-data">
                            <?php foreach ($a_kolom as $col) { ?>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <?php if ($col['kolom'] != ':no') : ?>
                                                <label for="<?= $col['kolom'] ?>"><?= $col['label'] ?></label>
                                            <?php endif; ?>
                                            <?php if (isset($col['type'])) {
                                                if ($col['type'] == 'S') { ?>
                                                    <select name="<?= $col['kolom'] ?>" id="<?= $col['kolom'] ?>" class="form-control">
                                                        <option value=""></option>
                                                        <?php foreach ($col['option'] as $key => $val) { ?>
                                                            <option value="<?= $key ?>" <?= $row[$col['kolom']] == $key ? 'selected' : '' ?>><?= $val ?></option>
                                                        <?php } ?>
                                                    </select>
                                                <?php
                                                } elseif ($col['type'] == 'A') {
                                                ?>
                                                    <textarea name="<?= $col['kolom'] ?>" id="<?= $col['kolom'] ?>" data-type="<?= $col['is_tiny'] ?>" data-id="<?= $col['is_tiny'] ?>" class="form-control" rows="3" placeholder="Masukkan <?= $col['label'] ?>"><?= $row[$col['kolom']] ?></textarea>
                                                <?php
                                                } elseif ($col['type'] == 'P') {
                                                ?>
                                                    <input type="password" name="<?= $col['kolom'] ?>" id="<?= $col['kolom'] ?>" value="<?= $row[$col['kolom']] ?>" class="form-control" />
                                                <?php
                                                } elseif ($col['type'] == 'N') {
                                                ?>
                                                    <input type="number" name="<?= $col['kolom'] ?>" id="<?= $col['kolom'] ?>" value="<?= $row[$col['kolom']] ?>" class="form-control" />
                                                <?php
                                                } elseif ($col['type'] == 'D') {
                                                ?>
                                                    <input type="date" name="<?= $col['kolom'] ?>" id="<?= $col['kolom'] ?>" value="<?= $row[$col['kolom']] ?>" class="form-control" />
                                                <?php
                                                } elseif ($col['type'] == 'F') {
                                                ?>
                                                    <div class="col sm-10">
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                <?php if (in_array(strtolower(explode('.', $row[$col['kolom']])[1]), array('jpg', 'png', 'jpeg'))) : ?>
                                                                    <img src="<?= base_url(substr($col['path'], 1) . $row[$col['kolom']]) ?>" class="img-thumbnail">
                                                                <?php else : ?>
                                                                    <img src="<?= base_url('assets/img/doc.png') ?>" class="img-thumbnail">
                                                                <?php endif ?>
                                                            </div>
                                                            <div class="col-sm-9">
                                                                <div class="custom-file">
                                                                    <input type="file" class="custom-file-input" id="<?= $col['kolom'] ?>" name="<?= $col['kolom'] ?>" />
                                                                    <label class=" custom-file-label" for="customFile"><?= $row[$col['kolom']] ?></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } else {
                                                ?>
                                                    <input type="text" name="<?= $col['kolom'] ?>" id="<?= $col['kolom'] ?>" value="<?= $row[$col['kolom']] ?>" class="form-control" />
                                                <?php
                                                }
                                                ?>
                                            <?php
                                            } else {
                                            ?>
                                                <?php if ($col['kolom'] != ':no') : ?>
                                                    <input type="text" name="<?= $col['kolom'] ?>" id="<?= $col['kolom'] ?>" value="<?= $row[$col['kolom']] ?>" class="form-control" />
                                                <?php endif ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <button type="submit" class="btn btn-success">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var idpengaturan = $('[data-type=ok]').attr('data-id');
    if (idpengaturan == 'ok') {
        tinymce.init({
            selector: 'textarea',
            theme: 'modern',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
            ],
            toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
            image_advtab: true,
            templates: [{
                    title: 'Test template 1',
                    content: 'Test 1'
                },
                {
                    title: 'Test template 2',
                    content: 'Test 2'
                }
            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ]
        });
    }
</script>