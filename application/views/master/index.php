            <!-- Main Content -->
            <div id="content">
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <button type="button" class="btn btn-primary mb-3" data-type="tambah">Tambah <?= $title; ?></button>
                            <div class="row">
                                <div class="col-lg-12">
                                    <?php if (validation_errors()) : ?>
                                        <div class="alert alert-danger" role="alert">
                                            <?= validation_errors(); ?>
                                        </div>
                                    <?php endif; ?>
                                    <?= $this->session->flashdata('message'); ?>
                                    <form method="post" id="form-list">
                                        <table class="table table-hover" id="tabelproduk">
                                            <thead>
                                                <tr>
                                                    <th scope="col">No</th>
                                                    <th scope="col">Nama Produk</th>
                                                    <th scope="col">Produk by</th>
                                                    <th scope="col">Stok</th>
                                                    <th scope="col">Status</th>
                                                    <th scope="col">Jenis</th>
                                                    <th scope="col">Harga</th>
                                                    <th scope="col">Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $i = 1; ?>
                                                <?php foreach ($produk as $s) : ?>
                                                    <tr>
                                                        <th scope="row"><?= $i; ?></th>
                                                        <td><?= $s['namaproduk']; ?></td>
                                                        <td><?= $s['namadesa']; ?></td>
                                                        <td><?= $s['stok']; ?></td>
                                                        <td><?= $s['status'] == 1 ? 'Tersedia' : 'Habis'; ?></td>
                                                        <td><?= $s['namajenisproduk']; ?></td>
                                                        <td><?= toRupiah($s['harga']); ?></td>
                                                        <td>
                                                            <button type="button" data-type="edit" data-id="<?= rawurlencode($s['idproduk']); ?>" class="btn btn-sm btn-info">Edit</button>
                                                            <button type="button" data-type="btndelete" data-id="<?= rawurlencode($s['idproduk']); ?>" class="btn btn-sm btn-danger">Delete</button>
                                                        </td>
                                                    </tr>
                                                    <?php $i++; ?>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                        <input type="hidden" name="act" id="act">
                                        <input type="hidden" name="key" id="key">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Modal -->
            <div class="modal fade" id="newMenuModal" tabindex="-1" role="dialog" aria-labelledby="newMenuModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="newMenuModalLabel">Tambah barang Baru</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="<?= base_url('master') ?>" method="post" id="modal_post">
                            <div class="modal-body">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="nis" name="nis" placeholder="NIS">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama">
                                </div>
                                <div class="form-group">
                                    <select name="jk" id="jk" class="form-control">
                                        <option value="L">Laki-Laki</option>
                                        <option value="P">Perempuan</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Alamat">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="notlp" name="notlp" placeholder="No Telepon">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" data-type="simpan" class="btn btn-success">Simpan</button>
                            </div>
                            <input type="hidden" name="act" id="act">
                            <input type="hidden" name="key" id="key">
                        </form>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="modal_upload" tabindex="-1" role="dialog" aria-labelledby="modal_upload" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <form action="<?= base_url('master') ?>" method="post" id="form_upload" enctype='multipart/form-data'>
                            <div class="modal-header">
                                <h5 class="modal-title">Import Siswa</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label">File upload</label>
                                    <div class="col-md-8">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="fileupload" name="fileupload">
                                            <label class="custom-file-label" for="fileupload">Pilih File</label>
                                        </div>
                                    </div>
                                </div>
                                <p>
                                    Silakan mendownload file template import disini : <button data-type="download" class="btn btn-sm btn-info">Download Template</button>
                                </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" data-type="import" class="btn btn-success">Import</button>
                            </div>
                            <input type="hidden" name="act" id="act">
                            <input type="hidden" name="key" id="key">
                        </form>
                    </div>
                </div>
            </div>

            <div class="modal" tabindex="-1" role="dialog" id="modal-delete">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Hapus Siswa</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Apakah anda ingin menghapus siswa?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" data-type="delete" data-id="" class="btn btn-danger">Hapus</button>
                        </div>
                    </div>
                </div>
            </div>

            <script>
                $('[data-type=simpan]').click(function() {
                    var act = $('#modal_post #act').val();
                    var key = $('#modal_post #key').val();
                    if (act == "") {
                        $('#modal_post #act').val('simpan');
                    }
                    $('#modal_post').submit();
                });

                $('[data-type=tambah]').click(function() {
                    var modal = $('#newMenuModal');
                    $('#modal_post')[0].reset();
                    modal.find('#newMenuModalLabel').html('Tambah <?= $title ?>');
                    modal.find('#nis').attr('disabled', false);
                    modal.modal();
                });

                $('[data-type=upload]').click(function() {
                    $('#modal_upload').modal();
                });

                $('[data-type=download]').click(function() {
                    $('#form_upload #act').val('download');
                    $('#form_upload').submit();
                });

                $('[data-type=import]').click(function() {
                    $('#form_upload #act').val('import');
                    $('#form_upload').submit();
                });

                $('[data-type=btndelete]').click(function() {
                    var id = $(this).attr('data-id');
                    $('#modal-delete').find('[data-type=delete]').attr('data-id', id);
                    $('#modal-delete').modal();
                });

                $('[data-type=delete]').click(function() {
                    var id = $(this).attr('data-id');
                    location.href = '<?= site_url('master/deleteSiswa?id=') ?>' + id;
                });

                $('[data-type=edit]').click(function() {
                    var id = $(this).attr('data-id');
                    Swal.showLoading();
                    xhrfGetData("<?= site_url('master/getSiswa/') ?>" + id, function(data) {
                        var modal = $('#newMenuModal');
                        modal.find('#newMenuModalLabel').html('Ubah <?= $title ?>');
                        modal.find('#nis').val(data.nis).attr('disabled', true);
                        modal.find('#nama').val(data.nama);
                        modal.find('#jk').val(data.jeniskelamin);
                        modal.find('#alamat').val(data.alamat);
                        modal.find('#notlp').val(data.notlp);
                        modal.find('#act').val('edit');
                        modal.find('#key').val(encodeURIComponent(data.nis));
                        Swal.close();
                        modal.modal();
                    });
                });

                $('#tabelproduk').DataTable();
            </script>