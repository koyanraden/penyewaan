<!-- Main Content -->
<div id="content">
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

        <div class="card shadow mb-4">
            <div class="card-body">
                <button class="btn btn-primary mb-3" data-type="tambah">Tambah <?= $title; ?></button>
                <div class="row">
                    <div class="col-md-12">
                        <?= $this->session->flashdata('message') ?>
                        <table class="table table-hover" id="tabeluser">
                            <thead>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Status</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                <?php foreach ($datauser as $us) : ?>
                                    <tr>
                                        <td><?= $i++ ?></td>
                                        <td><?= $us['nama'] ?></td>
                                        <td><?= $us['username'] ?></td>
                                        <td><?= $us['email'] ?></td>
                                        <td><?= $us['idpembeli'] == null ? 'Admin' : 'Pembeli' ?></td>
                                        <td><?= $us['is_aktif'] == 1 ? 'Aktif' : 'Non-Aktif' ?></td>
                                        <td>
                                            <button data-type="reset" data-id="<?= $us['iduser'] ?>" class="btn btn-sm btn-warning">Reset Pass</button>
                                            <button data-type="edit" data-id="<?= $us['iduser'] ?>" class="btn btn-sm btn-primary">Edit</button>
                                            <button data-type="hapus" data-id="<?= $us['iduser'] ?>" class="btn btn-sm btn-danger">Hapus</button>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="newMenuModal" tabindex="-1" role="dialog" aria-labelledby="newMenuModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newMenuModalLabel">Tambah <?= $title ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('masters/user') ?>" method="post" id="modal_post">
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="username" name="username" placeholder="Username">
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="password" name="password" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <select name="role" id="role" class="form-control">
                            <option value="1">Admin</option>
                            <option value="2">Pembeli</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <select name="status" id="status" class="form-control">
                            <option value="1">Aktif</option>
                            <option value="0">Tidak Aktif</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" data-type="simpan" class="btn btn-success">Simpan</button>
                </div>
                <input type="hidden" name="act" id="act">
                <input type="hidden" name="key" id="key">
            </form>
        </div>
    </div>
</div>
<div class="modal" tabindex="-1" role="dialog" id="modal-delete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hapus Siswa</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda ingin menghapus siswa?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" data-type="delete" data-id="" class="btn btn-danger">Hapus</button>
            </div>
        </div>
    </div>
</div>
<script>
    $('[data-type=tambah]').click(function() {
        var modal = $('#newMenuModal');
        $('#modal_post')[0].reset();
        modal.find('#newMenuModalLabel').html('Tambah <?= $title ?>');
        modal.find('#username').attr('disabled', false);
        modal.find('#password').attr('type', 'password');
        modal.modal();
    });

    $('[data-type=simpan]').click(function() {
        var act = $('#modal_post #act').val();
        var key = $('#modal_post #key').val();
        if (act == "") {
            $('#modal_post #act').val('simpan');
        }
        $('#modal_post').submit();
    });

    $('[data-type=hapus]').click(function() {
        var id = $(this).attr('data-id');
        $('#modal-delete').find('[data-type=delete]').attr('data-id', id);
        $('#modal-delete').modal();
    });

    $('[data-type=delete]').click(function() {
        var id = $(this).attr('data-id');
        location.href = '<?= site_url('masters/user/deleteuser/') ?>' + id;
    });

    $('[data-type=edit]').click(function() {
        var id = $(this).attr('data-id');
        Swal.showLoading();
        xhrfGetData("<?= site_url('masters/user/getUser/') ?>" + id, function(data) {
            var modal = $('#newMenuModal');
            var role = data.idpembeli == null ? 1 : 2;
            modal.find('#newMenuModalLabel').html('Ubah <?= $title ?>');
            modal.find('#nama').val(data.nama);
            modal.find('#username').val(data.username).attr('disabled', true);
            modal.find('#email').val(data.email);
            modal.find('#alamat').val(data.alamat);
            modal.find('#password').attr('type', 'hidden');
            modal.find('#role').val(role);
            modal.find('#status').val(data.is_aktif);
            modal.find('#act').val('edit');
            modal.find('#key').val(encodeURIComponent(data.iduser));
            Swal.close();
            modal.modal();
        });
    });

    $('#tabelproduk').DataTable();
</script>