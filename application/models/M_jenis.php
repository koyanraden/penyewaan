<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_jenis extends MY_Model
{
    protected $table = 'jenis_barang';
    protected $schema = '';
    public $key = 'idjenis';
    public $value = 'jenis';

    function __construct()
    {
        parent::__construct();
    }
}
