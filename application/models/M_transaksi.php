<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_transaksi extends MY_Model
{
    protected $table = 'transaksi';
    protected $schema = '';
    public $key = 'kodetransaksi';
    public $value = '';

    function __construct()
    {
        parent::__construct();
    }

    public function getTransaksi()
    {
        $query = "SELECT * FROM transaksi JOIN member USING(idmember)";
        return $this->db->query($query);
    }
}
