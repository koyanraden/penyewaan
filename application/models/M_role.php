<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_role extends MY_Model
{
    protected $table = 'userrole';
    protected $schema = '';
    public $key = 'idrole';
    public $value = 'role';

    function __construct()
    {
        parent::__construct();
    }
}
