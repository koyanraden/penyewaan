<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_barang extends MY_Model
{
    protected $table = 'barang';
    protected $schema = '';
    public $key = 'idbarang';
    public $value = 'namabarang';

    function __construct()
    {
        parent::__construct();
    }
}
